# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/vk-cs/vkcs" {
  version     = "0.1.14"
  constraints = "~> 0.1.12"
  hashes = [
    "h1:RzrePUuWwV38e4PnXt/MRAv/Cs5/vMqu0f1qhGHrzX0=",
  ]
}
