resource "vkcs_sharedfilesystem_sharenetwork" "sharenetwork" {
  name                = "app_sharenetwork"
  neutron_net_id      = vkcs_networking_network.app_network.id
  neutron_subnet_id   = vkcs_networking_subnet.app_subnet.id
  
  depends_on = [
    vkcs_networking_network.app_network,
    vkcs_networking_subnet.app_subnet,
    
  ]

}
resource "vkcs_sharedfilesystem_share" "nfs_share" { 
    availability_zone = "GZ1"
    name              = "nfs"
    description       = "NFS for wordpress"
    region            = "RegionOne"
    share_network_id  = "${vkcs_sharedfilesystem_sharenetwork.sharenetwork.id}"
    share_proto       = "NFS"
    share_type        = "default_share_type"
    size              = 10
}

resource "vkcs_sharedfilesystem_share_access" "share_access_app01" {
  share_id     = "${vkcs_sharedfilesystem_share.nfs_share.id}"
  access_type  = "ip"
  access_to    = "10.0.1.17" # ip инстанса app01
  access_level = "rw"

}
resource "vkcs_sharedfilesystem_share_access" "share_access_app02" {
  share_id     = "${vkcs_sharedfilesystem_share.nfs_share.id}"
  access_type  = "ip"
  access_to    = "10.0.1.18" # ip инстанса app02
  access_level = "rw"

}
