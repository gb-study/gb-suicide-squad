resource "vkcs_compute_instance" "app02" { #ВМ02 c Wordpress
    access_ip_v4      = "10.0.1.18"
    availability_zone = "MS1"
    flavor_id         = "25ae869c-be29-4840-8e12-99e046d2dbd4"
    flavor_name       = "Basic-1-2-20"
    image_id          = "Attempt to boot from volume - no image supplied"
    key_pair          = vkcs_compute_keypair.common_keypair.name
    metadata          = {
        "service_user_id" = "fb4a9ad111064b0ba8efa2fd5c0d8fe5"
    }
    name              = "app02"
    region            = "RegionOne"
    security_groups   = [
        "default", #используем стандартную группу безопасности
        "ssh+www", #используем стандартную группу безопасности
        "node_exporter"
    ]
    tags              = []

    block_device {
        boot_index            = 0
        delete_on_termination = true
        destination_type      = "volume"
        source_type           = "image"
        uuid                  = "d853edd0-27b3-4385-a380-248ac8e40956" #ubuntu 20.04
        volume_size           = 20
    }

    network {
        access_network = false
        fixed_ip_v4    = "10.0.1.18"
        name           = "app_network"
        uuid           = vkcs_networking_network.app_network.id #"b1eb864a-b2bc-40d4-a38b-09d96d405348"
    }
    depends_on = [
    vkcs_networking_subnet.app_subnet,
    vkcs_networking_router_interface.interface_to_app_subnet,
    vkcs_db_instance.db-master # зависит от кластера или инстанса БД
    # vkcs_sharedfilesystem_share.nfs_share
    # зависит и от NFS - дописать или share или instance
  ]
}