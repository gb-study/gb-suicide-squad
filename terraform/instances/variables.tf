variable "username" {
  type = string
}

variable "password" {
  type = string
  sensitive = true
}

variable "project_id" {
  type = string
}

variable "region" {
  type = string
}

variable "public-key-file" {
  type    = string
  default = "~/.ssh/id_rsa.pub" # взять готовый Public key из домашнего каталога ОС
}