data "vkcs_networking_network" "ext-net" {
    #admin_state_up        = true
    name                  = "ext-net"
    #private_dns_domain    = "openstacklocal."
    region                = "RegionOne"
    sdn                   = "neutron"
}

resource "vkcs_networking_network" "cp_network" {
    admin_state_up        = true
    name                  = "cp_network"
    port_security_enabled = true
    private_dns_domain    = "mcs.local."
    region                = "RegionOne"
    sdn                   = "neutron"
    tags                  = []
    vkcs_services_access  = false

    timeouts {}
}

resource "vkcs_networking_network" "app_network" {
    admin_state_up        = true
    name                  = "app_network"
    port_security_enabled = true
    private_dns_domain    = "mcs.local."
    region                = "RegionOne"
    sdn                   = "neutron"
    tags                  = []
    vkcs_services_access  = false

    timeouts {}
}
resource "vkcs_networking_subnet" "cp_subnet" {
    cidr            = "10.0.0.0/26"
    dns_nameservers = []
    enable_dhcp     = true
    gateway_ip      = "10.0.0.1"
    name            = "control_plane_subnet"
    network_id      = vkcs_networking_network.cp_network.id #"46f099d7-2893-48fe-a778-a74cd4268ea9"
    region          = "RegionOne"
    sdn             = "neutron"
    tags            = []

    allocation_pool {
        end   = "10.0.0.62"
        start = "10.0.0.2"
    }

    timeouts {}
}
resource "vkcs_networking_subnet" "app_subnet" {
    cidr            = "10.0.1.0/26"
    dns_nameservers = []
    enable_dhcp     = true
    gateway_ip      = "10.0.1.1"
    name            = "application_subnet"
    network_id      = vkcs_networking_network.app_network.id #"b1eb864a-b2bc-40d4-a38b-09d96d405348"
    region          = "RegionOne"
    sdn             = "neutron"
    tags            = []

    allocation_pool {
        end   = "10.0.1.62"
        start = "10.0.1.2"
    }

    timeouts {}
}
resource "vkcs_networking_router" "router" {
    admin_state_up      = true
    external_network_id = data.vkcs_networking_network.ext-net.id #"298117ae-3fa4-4109-9e08-8be5602be5a2"
    name                = "core_router"
    region              = "RegionOne"
    sdn                 = "neutron"
    tags                = []

    timeouts {}
}
resource "vkcs_networking_router_interface" "interface_to_cp_subnet" {
    #port_id   = "0ca0c25c-f82b-469f-8553-2544b4afd510"
    region    = "RegionOne"
    router_id = vkcs_networking_router.router.id #"ee09a705-8177-4931-8683-5bbfebb0f450"
    sdn       = "neutron"
    subnet_id = vkcs_networking_subnet.cp_subnet.id #"c77cba73-f06a-4326-ad6d-ae05f899cbe1"

    timeouts {}
}
resource "vkcs_networking_router_interface" "interface_to_app_subnet" {
    #port_id   = "54d9993e-7f50-496c-baf3-63edfc819660"
    region    = "RegionOne"
    router_id = vkcs_networking_router.router.id #"ee09a705-8177-4931-8683-5bbfebb0f450"
    sdn       = "neutron"
    subnet_id = vkcs_networking_subnet.app_subnet.id #"2a1cc5ff-b013-4dbc-a978-9fac1371c47f"

    timeouts {}
}
# Grafana rules:
resource "vkcs_networking_secgroup" "grafana" {
  name        = "grafana"
  description = "SG for Grafana"
}
resource "vkcs_networking_secgroup_rule" "grafana_rule" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 3000
  port_range_max    = 3000
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.grafana.id
}
# Prometheus rules:
resource "vkcs_networking_secgroup" "prometheus" {
  name        = "prometheus"
  description = "SG for Prometheus"
}
resource "vkcs_networking_secgroup_rule" "prometheus_rule" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9090
  port_range_max    = 9090
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.prometheus.id
}
# Node exporter rules:
resource "vkcs_networking_secgroup" "node_exporter" {
  name        = "node_exporter"
  description = "SG for node exporter"
}
resource "vkcs_networking_secgroup_rule" "node_exporter_rule" {
  direction         = "ingress"
  protocol          = "tcp"
  port_range_min    = 9100
  port_range_max    = 9100
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.node_exporter.id
}


/*
resource "vkcs_networking_secgroup" "default" {
    description = "Default security group"
    name        = "default"
    region      = "RegionOne"
    sdn         = "neutron"
    tags        = []

    timeouts {}
}
resource "vkcs_networking_secgroup" "ssh" {
    name     = "ssh"
    region   = "RegionOne"
    sdn      = "neutron"
    tags     = []

    timeouts {}
}
resource "vkcs_networking_secgroup" "ssh_www" {
    name     = "ssh+www"
    region   = "RegionOne"
    sdn      = "neutron"
    tags     = []

    timeouts {}
}
resource "vkcs_networking_secgroup" "SecGroup_db" {
    description = "Security Group for instance db instance_id a82b9f2e-c820-46df-9a0a-8b8df8b2d316"
    name        = "SecGroup_db"
    region      = "RegionOne"
    sdn         = "neutron"
    tags        = []

    timeouts {}
}
/* если потребуется отдельное правило на порты:
resource "vkcs_networking_secgroup_rule" "имя_правила" {

   direction = "ingress"
   port_range_max = 22
   port_range_min = 22
   protocol = "tcp"
   remote_ip_prefix = "0.0.0.0/0"
   security_group_id = vkcs_networking_secgroup.имя_сек_группы.id
   description = "ssh ingress from everywhere"

}*/