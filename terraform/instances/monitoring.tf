resource "vkcs_compute_instance" "monitoring" { #ВМ для мониторинга (Prometheus)
    access_ip_v4      = "10.0.0.20"
    availability_zone = "MS1"
    flavor_id         = "25ae869c-be29-4840-8e12-99e046d2dbd4"
    flavor_name       = "Basic-1-2-20"
    image_id          = "Attempt to boot from volume - no image supplied"
    key_pair          = vkcs_compute_keypair.common_keypair.name
    metadata          = {
        "service_user_id" = "e6d32d07e0c04c378903df3a8be068c2"
    }
    name              = "monitoring"
    region            = "RegionOne"
    security_groups   = [
        "default", #используем стандартную группу безопасности
        "ssh+www", #используем стандартную группу безопасности
        "prometheus"
    ]
    tags              = []

    block_device {
        boot_index            = 0
        delete_on_termination = true
        destination_type      = "volume"
        source_type           = "image"
        uuid                  = "d853edd0-27b3-4385-a380-248ac8e40956" #ubuntu 20.04
        volume_size           = 20
    }

    network {
        access_network = false
        fixed_ip_v4    = "10.0.0.20"
        name           = "cp_network"
        uuid           = vkcs_networking_network.cp_network.id #"46f099d7-2893-48fe-a778-a74cd4268ea9"
    }
    depends_on = [
    vkcs_networking_subnet.cp_subnet,
    vkcs_networking_router_interface.interface_to_cp_subnet,
    
  ]
}