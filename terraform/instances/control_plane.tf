resource "vkcs_compute_instance" "control_plane" { #ВМ администрирования
    name              = "control-plane"
    access_ip_v4      = "10.0.0.9"
    availability_zone = "MS1"
    flavor_id         = "25ae869c-be29-4840-8e12-99e046d2dbd4"
    flavor_name       = "Basic-1-2-20"
    image_id          = "Attempt to boot from volume - no image supplied"
    key_pair          = vkcs_compute_keypair.common_keypair.name
    metadata          = {
        "service_user_id" = "60cd45340c384de88d2c0e7e5e70b0c0"
    }
    region            = "RegionOne"
    security_groups   = [
        "default", #используем стандартную группу безопасности
        "ssh", #используем стандартную группу безопасности
        "grafana"
    ]
    tags              = []

    block_device {
        boot_index            = 0
        delete_on_termination = true
        destination_type      = "volume"
        source_type           = "image"
        uuid                  = "d853edd0-27b3-4385-a380-248ac8e40956" #ubuntu 20.04
        volume_size           = 20
    }

    network {
        access_network = false
        fixed_ip_v4    = "10.0.0.9"
        name           = "cp_network"
        uuid           = vkcs_networking_network.cp_network.id #"46f099d7-2893-48fe-a778-a74cd4268ea9"
    }
    depends_on = [
    vkcs_networking_subnet.cp_subnet,
    vkcs_networking_router_interface.interface_to_cp_subnet,
  ]
}
resource "vkcs_networking_floatingip" "fip" { # запросим публичный IP для Control plane
  pool = data.vkcs_networking_network.ext-net.name
}

resource "vkcs_compute_floatingip_associate" "control_fip" { # привяжем публичный IP
  floating_ip = vkcs_networking_floatingip.fip.address
  instance_id = vkcs_compute_instance.control_plane.id
}
output "cp_external_ip" { # выведем полученный публичный IP инстанса Control plane
  value = vkcs_networking_floatingip.fip.address
}