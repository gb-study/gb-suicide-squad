variable "db-instance-flavor" { 
  type    = string
  default = "Standard-2-8-50" #минимальный допустимый рецепт для DBaaS, хотя по ТЗ достаточно 1-2-20
}

data "vkcs_compute_flavor" "db" {
  name = var.db-instance-flavor
}
/*
data "vkcs_networking_network" "app_network" { # уже готовая сеть из манифеста network.tf
  name           = "app_network"
}
*/
resource "vkcs_db_instance" "db-master" { # DBaaS мастер-реплика из двух инстансов
  name        = "mysql_master"
  #cluster_size = 3 # для кластера минимально 3 инстанса, в итоге превышаем кол-во инстансов на проект (6шт)
  keypair     = vkcs_compute_keypair.common_keypair.name
  flavor_id   = data.vkcs_compute_flavor.db.id
  size        = 20
  volume_type = "ceph-ssd"
  availability_zone = "MS1"
  
  datastore {
    type    = "mysql"
    version = "8.0"
  }
  network {
    fixed_ip_v4    = "10.0.1.5"
    uuid = vkcs_networking_network.app_network.id
  }
  
  capabilities { # опционально, не уверен, что надо через TF ставить exporter
    name = "node_exporter"
    settings = {
      "listen_port" : "9100"
    }
  }
  depends_on = [
    vkcs_networking_subnet.app_subnet,
    vkcs_networking_router_interface.interface_to_app_subnet
  ]
}
resource "vkcs_db_instance" "db-replica" { # DBaaS мастер-реплика из двух инстансов
  name        = "mysql_replica"
  #cluster_size = 3 # для кластера минимально 3 инстанса, в итоге превышаем кол-во инстансов на проект (6шт)
  keypair     = vkcs_compute_keypair.common_keypair.name
  flavor_id   = data.vkcs_compute_flavor.db.id
  size        = 20
  volume_type = "ceph-ssd"
  #availability_zone = "MS1"
  replica_of  = vkcs_db_instance.db-master.id
  datastore {
    type    = "mysql"
    version = "8.0"
  }
  network {
    fixed_ip_v4    = "10.0.1.4"
    uuid = vkcs_networking_network.app_network.id
  }
  
  depends_on = [
    vkcs_networking_subnet.app_subnet,
    vkcs_networking_router_interface.interface_to_app_subnet
  ]
}
