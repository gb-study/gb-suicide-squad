resource "vkcs_networking_floatingip" "lb_flip" { # запросим плавающий публичный ip для LB
  pool = data.vkcs_networking_network.ext-net.name
}
resource "vkcs_networking_floatingip_associate" "wp_fip" { # привяжем flip к LB
  floating_ip = vkcs_networking_floatingip.lb_flip.address
  port_id     = vkcs_lb_loadbalancer.public_lb.vip_port_id
}

resource "vkcs_lb_loadbalancer" "public_lb" { # сам LB
  name          = "Public LB"
  vip_subnet_id = vkcs_networking_subnet.app_subnet.id
  
  depends_on = [
    vkcs_compute_instance.app01,
    vkcs_compute_instance.app02
  ]
}

resource "vkcs_lb_listener" "project_lb_listener" { # listener resource
  loadbalancer_id = vkcs_lb_loadbalancer.public_lb.id
  name            = "LB listener"
  protocol        = "HTTP"
  protocol_port   = 80
  
  insert_headers = {
		X-Forwarded-For = "true"
  }
}
resource "vkcs_lb_pool" "pool_1" { # пул LB
	listener_id = vkcs_lb_listener.project_lb_listener.id
  name        = "lb_pool"
  protocol    = "HTTP"
	lb_method   = "LEAST_CONNECTIONS"
}

resource "vkcs_lb_member" "app01" { # инстанс app01 c WP
	address       = vkcs_compute_instance.app01.access_ip_v4
	pool_id       = vkcs_lb_pool.pool_1.id
	protocol_port = 80
  subnet_id     = vkcs_networking_subnet.app_subnet.id
  weight        = 10
  depends_on = [
    vkcs_compute_instance.app01
  ]
}
resource "vkcs_lb_member" "app02" { # инстанс app02 c WP
	address       = vkcs_compute_instance.app02.access_ip_v4
	pool_id       = vkcs_lb_pool.pool_1.id
	protocol_port = 80
  subnet_id     = vkcs_networking_subnet.app_subnet.id
  weight        = 10
  depends_on = [
    vkcs_compute_instance.app02
  ]
}
resource "vkcs_lb_monitor" "monitor_lb" { # проверка доступности LB
	pool_id     = vkcs_lb_pool.pool_1.id
	type        = "PING"
	delay       = 20
	timeout     = 10
	max_retries = 5
}
output "lb_external_ip" { # выведем полученный публичный IP Load Balancer
  value = vkcs_networking_floatingip.lb_flip.address
}